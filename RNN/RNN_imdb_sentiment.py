# -*- coding: utf-8 -*-
"""
Created on Sun Apr 12 10:35:21 2020

@author: 607144701
"""

#importing libraries
from __future__ import print_function
import numpy as np
from sklearn.metrics import accuracy_score
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Dropout, Embedding, LSTM, Bidirectional
from keras.datasets import imdb
from keras.models import model_from_json
from keras.models import load_model

max_features = 20000
# cut texts after this number of words
# (among top max_features most common words)
maxlen = 100
batch_size = 32

print('Loading data...')
load_old = np.load


np_load_old = np.load
np.load = lambda *a,**k: np_load_old(*a,allow_pickle=True)
(x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=max_features)
#x_train.shape
print(len(x_train), 'train sequences')
print(len(x_test), 'test sequences')
word_index = imdb.get_word_index()   
#len(word_index)

#x_train[0]
#word_index['bonestell']
#for index,value in word_index.items():
#    print(value)
#int_to_vocab = {c: i for i, c in enumerate(word_index)}
#chars = np.array([value:index for index,value in word_index.items() for value in x_train[0]], dtype=np.int32)
#chars = [index for index,value in word_index.items() if value in x_train[0] ]

reverse_word_index = {value:index for index,value in word_index.items()}
#
#chars = [value for index,value in reverse_word_index.items() if index in x_train[0] ]

sentence = ''
for idx in x_train[0]:
    sentence = sentence + reverse_word_index.get(idx)+ ' '
#chars = reverse_word_index.get(idx for idx in x_train[0])
#for st in x_train[0]:
#    if st in reverse_word_index.items():
#        print(st)
#        print(key)
        
        
        
#list(word_index.items())
#word_index.items().
#for key,value in  word_index.items():
#    for var in x_train[0]:
#        if value == var:
#            print(key)

#for var in x_train[0]:
#    print(list(word_index.keys()).index(var))
#    word_index.values()


def getKeysByValues(dictOfElements, listOfValues):
    listOfKeys = list()
    listOfItems = dictOfElements.items()
    for item  in listOfItems:
        if item[1] in listOfValues:
            listOfKeys.append(item[0])
    return  listOfKeys 

def getKeysBySingleValue(dictOfElements, var):
    listOfKeys = list()
    listOfItems = dictOfElements.items()
    for var in listOfItems:
        if item[1] in listOfValues:
            listOfKeys.append(item[0])
    return  listOfKeys 
#def getKeysByValuesSorted(dictOfElements, listOfValues):
#    listOfKeys = {}
#    listOfItems = dictOfElements.items()
#    for item  in listOfItems:
#        if item[1] in listOfValues:
#            listOfKeys.update({item[1].index,item[0]})
#    return  listOfKeys 


#sentence = getKeysByValuesSorted(word_index,x_train[0])
##        print(value)
##sentence= [index for index,value in word_index.items() if value in [id for id in x_train[0] ] ]
##idx = [index for id in x_train[0] for index,id in word_index.items() ] 
#sentence=[value for index,value in word_index.items()]
#idx = [index for id in x_train[0]]



print('Pad sequences (samples x time)')
x_train = sequence.pad_sequences(x_train, maxlen=maxlen)
x_train.shape
x_test = sequence.pad_sequences(x_test, maxlen=maxlen)
print('x_train shape:', x_train.shape)
print('x_test shape:', x_test.shape)
y_train = np.array(y_train)
y_test = np.array(y_test)

model = Sequential()
model.add(Embedding(max_features, 128, input_length=maxlen))
model.add(Bidirectional(LSTM(64)))
model.add(Dropout(0.5))
model.add(Dense(1, activation='sigmoid'))

# try using different optimizers and different optimizer configs
model.compile('adam', 'binary_crossentropy', metrics=['accuracy'])

print('Train...')
model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=4,
          validation_data=[x_test, y_test])

#Save 
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
model.save_weights("model.h5")

#Retrieve
json_file = open('model.json', 'r')

loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
new_model= loaded_model.load_weights("model.h5")

y_pred = model.predict(x_test)

y_pred.shape
y_test.shape
y_test = y_test.astype(np.float32)
y_test.dtype
y_pred.dtype
y_test = y_test.reshape(25000, 1)

print(accuracy_score(y_pred.round(), y_test))



# Input text review  and conversion of vector space 



#p_input = "It is a bad show and people deserves more than what it was been .Very poor potray of character ";
#p_input = "Brilliant movie with spectacular screeplay. A must watch movie. Very good   ";
#p_input = "this isnt the comedic robin williams nor is it the quirky insane robin williams of recent thriller fame this is a hybrid of the classic drama without over dramatization mixed with robins new love of the thriller but this isnt a thriller per se this is more a mystery suspense vehicle through which williams attempts to locate a sick boy and his keeper also starring sandra oh and rory culkin this suspense drama plays pretty much like a news report until williams character gets close to achieving his goal i must say that i was highly entertained though this movie fails to teach guide inspect or amuse it felt more like i was watching a guy williams as he was actually performing the actions from a third person perspective in other words it felt real and i was able to subscribe to the premise of the story all in all its worth a watch though its definitely not friday saturday night fare it rates a   from the fiend ";
#p_input = "He knows that pain and that’s why he could showcase it on the silver screen. The attempt to showcase the struggle and pain of Kashmiri Pandits who have gone through this tough time and are living in other parts of the country now will surely make into watchlist of audience. Overall, Shikara is good one time watch for another audience but very special for some people"
#word_index.get('comedic').dtype
p_input ="Arthur’s laugh is hugely significant, as it works as the emotional core of the film. Sometimes it is infectious as it engenders nervous laughter from the viewer, but it is also recognizable as a scream of anguish. When this anguish starts erupting in other ways, the progression is shocking and even horrifying. Perhaps most disturbing of all, the sudden eruptions of violence are also understandable. Cinema audiences have laughed at the outbursts of violence, bloody and brutal as they are, and also admonished each other: “It’s not funny!” However, as Arthur points out at one point, everyone around him is awful, so perhaps it is funny to see awful people hurt and killed, at least in a movie. If we laugh, it is not at Arthur but somehow with him, sharing his pain and frustration (rather than mirth) and somehow expressing it similarly. And this is even more disturbing, as it suggests the ease with which our own civility may slip away, at least in the safe environs of a movie theater. This is a great strength of Joker, reaching into the viewer’s own soul and drawing out parts we would rather not acknowledge"
listOfWords = p_input.split(' ')
x_custom = np.array([0])
maxlen = 100
for i in listOfWords:
    try:
        
        if word_index.get(i) < 20000:
             x_custom = np.append(x_custom,word_index.get(i))
        else:
             x_custom = np.append(x_custom,0)
    except :
         x_custom = np.append(x_custom,0)
   

x_custom.size
x_custom = x_custom.astype(np.float)
x_custom = np.nan_to_num(x_custom)
pslice = round(len(x_custom)/maxlen)
if pslice == 0:
    pslice = 1
x_custom = x_custom.astype(np.int32)
x_custom = x_custom.reshape(1,x_custom.size)
x_custom = sequence.pad_sequences(x_custom, maxlen=pslice*maxlen)
x_custom= x_custom.reshape(pslice,maxlen)
#x_custom = sequence.pad_sequences(x_custom, maxlen=maxlen)
#x_custom = x_custom.astype(np.int32)
x_custom.shape
x_test.shape
y_custom_pred = model.predict(x_custom)
#reverse_word_index.get(61358)
#len(reverse_word_index)











