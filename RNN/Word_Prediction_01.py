# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 19:14:10 2020

@author: 607144701
"""

import time
from collections import namedtuple

import numpy as np
import tensorflow as tf
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding
from keras.layers import LSTM
from keras.preprocessing import sequence
from keras.optimizers import RMSprop
import random
tf.VERSION
with open('C:/Pinak/Mygit/mycode/RNN/anna.txt', 'r') as f:
    text=f.read()
words = text.split(' ')
vocab = set(words)
vocab_to_int = {c: i for i, c in enumerate(vocab)}
print(vocab_to_int)
int_to_vocab = dict(enumerate(vocab))
chars = np.array([vocab_to_int[c] for c in words], dtype=np.int32)
print(chars)
print(text[:100])
print(chars[:100])
print(np.max(chars)+1)

def split_data(chars, batch_size, num_steps, split_frac=0.9):
    """ 
    Split character data into training and validation sets, inputs and targets for each set.
    
    Arguments
    ---------
    chars: character array
    batch_size: Size of examples in each of batch
    num_steps: Number of sequence steps to keep in the input and pass to the network
    split_frac: Fraction of batches to keep in the training set
    
    
    Returns train_x, train_y, val_x, val_y
    """
    
    slice_size = batch_size * num_steps
    n_batches = int(len(chars) / slice_size)
    
    # Drop the last few characters to make only full batches
    x = chars[: n_batches*slice_size]
    y = chars[1: n_batches*slice_size + 1]
    
    # Split the data into batch_size slices, then stack them into a 2D matrix 
    x = np.stack(np.split(x, batch_size))
    y = np.stack(np.split(y, batch_size))
    
    # Now x and y are arrays with dimensions batch_size x n_batches*num_steps
    
    # Split into training and validation sets, keep the first split_frac batches for training
    split_idx = int(n_batches*split_frac)
    train_x, train_y= x[:, :split_idx*num_steps], y[:, :split_idx*num_steps]
    val_x, val_y = x[:, split_idx*num_steps:], y[:, split_idx*num_steps:]
    
    return train_x, train_y, val_x, val_y


train_x, train_y, val_x, val_y = split_data(chars, 10, 50)

train_x.shape
train_x.size

maxlen = 128

batch_size = 100
num_steps = 100 
lstm_size = 128
num_layers = 2
learning_rate = 0.001
keep_prob = 10.5

#!mkdir checkpoints
        

#epochs = 20

# len(vocab_to_int)
# len(chars)

pslice = round(train_x.size/maxlen)
train_x = train_x.reshape(1,train_x.size)
train_x = sequence.pad_sequences(train_x, maxlen=pslice*maxlen)
train_x= train_x.reshape(pslice,maxlen)

pslice = round(train_y.size/maxlen)
train_y = train_y.reshape(1,train_y.size)
train_y = sequence.pad_sequences(train_y, maxlen=pslice*maxlen)
train_y= train_y.reshape(pslice,maxlen)

pslice = round(val_x.size/maxlen)
val_x = val_x.reshape(1,val_x.size)
val_x = sequence.pad_sequences(val_x, maxlen=pslice*maxlen)
val_x= val_x.reshape(pslice,maxlen)


pslice = round(val_y.size/maxlen)
val_y = val_y.reshape(1,val_y.size)
val_y = sequence.pad_sequences(val_y, maxlen=pslice*maxlen)
val_y= val_x.reshape(pslice,maxlen)

#train_x = sequence.pad_sequences(train_x, maxlen=maxlen)
#train_y = sequence.pad_sequences(train_y, maxlen=maxlen)
#val_x = sequence.pad_sequences(val_x, maxlen=maxlen)
#val_y = sequence.pad_sequences(val_y, maxlen=maxlen)


model = Sequential()
model.add(Embedding(len(vocab_to_int), maxlen))
model.add(LSTM(128, dropout=0.2, recurrent_dropout=0.2))
model.add(Dense(128, activation='softmax'))
optimizer = RMSprop(learning_rate=0.01)
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

print('Train...')
model.fit(train_x, train_y,
          batch_size=batch_size,
          epochs=20,
          validation_data=(train_x, train_y))

y_custom_pred = model.predict(train_x)
#model
preds = model.predict(val_x, verbose=1)[0]
preds.shape

char_indices = dict((c, i) for i, c in enumerate(words))
indices_char = dict((i, c) for i, c in enumerate(words))

# def sample(preds, temperature=1.0):
#     # helper function to sample an index from a probability array
#     preds = np.asarray(preds).astype('float64')
#     preds = np.log(preds) / temperature
#     exp_preds = np.exp(preds)
#     preds = exp_preds / np.sum(exp_preds)
#     probas = np.random.multinomial(1, preds, 1)
#     return np.argmax(probas)

# len(words)
# len(chars)
# sentence= words[0: 5]
# generated = ''
# generated += words[0]
# x_pred = np.zeros((1, len(chars)))
# # x_pred.shape

# x_pred[0, char_indices[x_var]] = 1.
# preds = model.predict(x_pred, verbose=0)[0]
#for diversity in [0.2, 0.5, 1.0, 1.2]:


def sample(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)

x_pred = np.zeros((1, len(chars)))
x_var = "alike"
char_indices[x_var]
x_pred[0, char_indices[x_var]] = 1.
#for diversity in [0.2, 0.5, 1.0, 1.2]:
for i in range(40):
    next_index = sample(preds)
    next_char = indices_char[next_index]
    x_pred[0, char_indices[x_var]] = 1.
    x_var = next_char
    print(x_var)
    preds = model.predict(x_pred, verbose=0)[0]







def generate_sentence():
    
    start_index = random.randint(0, len(words) - maxlen - 1)
    for diversity in [0.2, 0.5, 1.0, 1.2]:
        
        print('----- diversity:', diversity)

        generated = ''
        sentence = words[start_index]
        generated +=  words[start_index]
        print('----- Generating with seed: "' + sentence + '"')
        print(generated)
        for i in range(400):
            
            x_pred = np.zeros((1, maxlen, len(chars)))
            for t, char in enumerate(sentence):
                x_pred[0, t, char_indices[char]] = 1.

            preds = model.predict(x_pred, verbose=0)[0]
            next_index = sample(preds, diversity)
            next_char = indices_char[next_index]

            sentence = sentence[1:] + next_char

            print(next_char)


generate_sentence()
samp = sample( 2000, lstm_size, len(vocab), prime="Anna")
print(samp)