package com.example.demo.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component("productCatalogDAO")
public class ProductCatalogDAOImpl implements ProductCatalogDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public Map<String, Object> getProductCatalog()
	{
		return new ProductCatalogSP(jdbcTemplate,"PRODUCT_CATALOG").execute();
	}

}
