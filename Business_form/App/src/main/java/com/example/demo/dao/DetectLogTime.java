package com.example.demo.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

public class DetectLogTime {
	
	 class IdDetails {
		 private int id;

		 private int timeLapsed;
		int getTimeLapsed() {
			return timeLapsed;
		}
		void setTimeLapsed(int timeLapsed) {
			this.timeLapsed = timeLapsed;
		}
	
		int getId() {
			return id;
		}
		void setId(int id) {
			this.id = id;
		}
	 }
	
	  public static List<String> processLogs(List<String> logs, int maxSpan) {

		    // Write your code here
	       /* for (int i = 0; i < logs.size(); i++)   
	        {  
	        System.out.println("Hi there "+logs.get(i));  
	        }   */
		  List<String> result = new ArrayList();
		  Set<String> set = new HashSet<String>();
		  for (int i = 0; i < logs.size(); i++)   
	        {  
			  
			  String[] arrOfStr = logs.get(i).split(" ");
			  set.add(arrOfStr[0]);
			  /*for (String a : arrOfStr)
		            System.out.println(a);*/
	        } 
		   // System.out.println(set);
		    
		    Iterator iter = set.iterator();
		    while (iter.hasNext()) {
		        String id = (String) iter.next();
		        int timeStart = 0;
		        int timeEnd = 0 ;
		        int timeElapsed = 0;
		        for (int i = 0; i < logs.size(); i++)   
		        {  
				  
				  String[] arrOfStr = logs.get(i).split(" ");
				  if(arrOfStr[0].equals(id)) 
				  {
					  if (arrOfStr[2].equals("sign-in"))
					  {
						  timeStart = Integer.parseInt(arrOfStr[1]);
						  
					  }
					  else if(arrOfStr[2].equals("sign-out"))
					  {
						  timeEnd = Integer.parseInt(arrOfStr[1]);
					  }
					  else
						  throw new RuntimeException();
					  
				  }
				  /*for (String a : arrOfStr)
			            System.out.println(a);*/
		         } 
		        timeElapsed = timeEnd - timeStart;
		        if (timeElapsed <= maxSpan)
		        {
		        	result.add(String.valueOf(id));
		        }
		    }
	        return result;
	        
	 }

	  public static void main(String[] args) throws IOException {
		  List<String> str = Arrays.asList("99 1 sign-in","99 7 sign-out","100 1 sign-in","100 9 sign-out");
		  List<String> result = DetectLogTime.processLogs(str, 10);
	    }
}
