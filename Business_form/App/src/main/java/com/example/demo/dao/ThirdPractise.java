package com.example.demo.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class ThirdPractise {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
	    StringBuffer buffer = new StringBuffer();
	    try {
	    	BufferedReader brNext = new BufferedReader(new InputStreamReader(System.in));
	        int ch;
	        String s = null;
	        while ((ch = brNext.read()) > -1) {
	        	if ((s = brNext.readLine()) != null)
			{
				break;
			}
			   buffer.append((char)ch);
	        }
	       
	        brNext.close();
	        System.out.println(buffer.toString());
	    } 
	    catch (IOException e) {
	        e.printStackTrace();
	      
	    }

	}

}
