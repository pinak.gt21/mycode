package com.example.demo.dao;

import java.io.IOException;

public class Vowel {
	public static void main(String args[]) throws IOException {
		
		String input  = "3JHAEOYUqpl8X";
		boolean isVowwel = detectVowel(input);
		System.out.println(isVowwel);
	}
	
	public static boolean detectVowel( String input) {
		char[] vowel = {'a','e','i','o','u'};
		
		for ( int i =0 ; i < vowel.length; i ++ )
		{

			if(input.toLowerCase().indexOf(vowel[i]) >0)
			{
				return true;
			}
		}
		return false;
	}
	
    public static char[] stringToCharPrnt(String input)
    {
        String s = "Techie Delight";
 
        // convert string to `char[]` array
        char[] chars = s.toCharArray();
 
        // iterate over `char[]` array using enhanced for-loop
        for (char ch: chars) {
            System.out.print(ch);
        }
        
        return chars;
    }
	
}
