package com.example.demo.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ProductCatalogDAO;

@Service("productCatalogService")
public class ProductCatalogServiceImpl implements ProductCatalogService{

	@Autowired
	private ProductCatalogDAO productCatalogDAO;

	@Override
	public Map<String, Object> getProductCatalog()
	{
		return productCatalogDAO.getProductCatalog();
	}
}