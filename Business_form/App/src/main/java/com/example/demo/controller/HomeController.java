package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.ProductCatalogService;

@RestController
public class HomeController {
	
	@Autowired
	ProductCatalogService productCatalogService;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@RequestMapping(value ="/product",produces = {MediaType.APPLICATION_JSON_VALUE} )
	@ResponseBody
	public Map<String,Object> aiCatalog() {
		
		Map<String,Object> dummyMap = productCatalogService.getProductCatalog();
		return dummyMap;
	}

}
