package com.example.demo.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.example.demo.model.ProductCatalog;

import oracle.jdbc.OracleTypes;

public class ProductCatalogSP extends StoredProcedure{

	protected ProductCatalogSP(JdbcTemplate jdbcTemplate, String sql) {
		super(jdbcTemplate,sql);
		
		declareParameter(new SqlOutParameter("o_product_list", OracleTypes.CURSOR, new ResultSetExtractor<Object>()
		{
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				return extractDetailsFromRs(rs);
			}
		}));
		compile();
	}
	
	private static ArrayList<ProductCatalog> extractDetailsFromRs(ResultSet rs) throws SQLException {
		ProductCatalog productCatalog = new ProductCatalog();
		ArrayList <ProductCatalog> productList = new ArrayList<ProductCatalog>();
		while(rs.next()) {
			productCatalog.setProductName(rs.getString("I_USER"));
			productCatalog.setProductDescription(rs.getString("I_DESC"));
			productList.add(productCatalog);
		}
		return productList;
	}
	
	public Map<String,Object> execute()
	{
		Map<String, Object> result = new HashMap();
		Map<String, Object> inParams = new HashMap();
		Map<String, Object> outParams = execute(inParams);
		result.put("productList", outParams.get("o_product_list"));
		return result;
		
	}
}
