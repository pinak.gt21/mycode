package com.example.demo.dao;

import java.util.Map;

public interface ProductCatalogDAO {

	public Map<String, Object> getProductCatalog();
}
