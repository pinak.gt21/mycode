package com.example.demo.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SecondPractise {

	public static void main(String args[]) throws IOException {
		/*
		 * int mat[][] = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 } };
		 * print2D(mat);
		 */
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = br.readLine();
		BufferedReader brNext = new BufferedReader(new InputStreamReader(System.in));
		String sNext = brNext.readLine();
		if (isStringOnlyAlphabet(s) && isStringOnlyAlphabet(sNext)) {
			boolean check = isAnagramSort(s, sNext);
			if (check)
				System.out.println("Yes");
			else
				System.out.println("No");
		} else
			System.out.println("No");

	}

	public static boolean isStringOnlyAlphabet(String str) {
		if (null == str || str.isEmpty())
			return false;
		else
			return ((!str.equals("")) && (str != null) && (str.matches("^[A-Z]*$")));
	}

	public static String compareMap(Map<String, Long> map1, Map<String, Long> map2) {

		if (map1.size() != map2.size()) 
			return "No";
		if (map1 == null || map2 == null)
			return "No";

		for (String ch1 : map1.keySet()) {

			if (map1.get(ch1) != map2.get(ch1))
				return "No";

		}

		for (String ch2 : map2.keySet()) {

			if (map2.get(ch2) != map1.get(ch2))
				return "No";

		}

		return "No";

	}

	public static Map<String, Long> charCount(String str) {
		if (null == str || str.isEmpty())
			return new HashMap();
		else {
			Map<String, Long> charCount = IntStream.range(0, str.length()).mapToObj(i -> str.substring(i, i + 1))
					.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
			return charCount;
		}

	}

	public static boolean isAnagramSort(String string1, String string2) {
		if (string1.length() != string2.length()) {
			return false;
		}
		char[] a1 = string1.toCharArray();
		char[] a2 = string2.toCharArray();
		Arrays.sort(a1);
		Arrays.sort(a2);
		return Arrays.equals(a1, a2);
	}

}
