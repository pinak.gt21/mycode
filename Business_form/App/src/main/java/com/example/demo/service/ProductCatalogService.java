package com.example.demo.service;

import java.util.Map;

public interface ProductCatalogService {
	
	public Map<String, Object> getProductCatalog();

}
