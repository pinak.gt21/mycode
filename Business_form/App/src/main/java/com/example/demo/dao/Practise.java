package com.example.demo.dao;
/* IMPORTANT: Multiple classes and nested static classes are supported */

/*
 * uncomment this if you want to read input.
//imports for BufferedReader
import java.io.BufferedReader;
import java.io.InputStreamReader;

//import for Scanner and other utility classes
import java.util.*;
*/


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;


// Warning: Printing unwanted or ill-formatted data to output will cause the test cases to fail

public class Practise {
    public static void main(String args[] ) throws Exception {
    	
    	outer: while (1==1)
    	{
    		
			Scanner reader = new Scanner(System.in);
			//System.out.println("Enter Number of Elements: ");
			int n = -1;
			if (reader.hasNextInt())
			n =reader.nextInt();
			//reader.close();
			if (n > 10 || n < 0 )
			{
				//System.out.println(-1);
				 break outer;
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			//System.out.println("Enter Space seperated Number: \n");
			
			String s = br.readLine();
			//System.out.println("Number"+n+" and string "+s);
			if (null ==s || s.isEmpty())
			{
				System.out.println(-1);
				 break outer;
			}
			String[] splited = s.split(" ");
			if (splited.length != n)
			{
				//System.out.println(-1);
				 break outer;
			}
			int[] intdata = stringToIntArray(splited);
			int finalNum = 0;
			try
			{
				finalNum =getSmallest(intdata,n);
				//Arrays.sort(intdata);

			}
			catch( Exception e)
			{
				//System.out.println(-1);
				//throw new Exception("Exceed 100");
				 break outer;
			}
			System.out.println(finalNum);

    	}

    }

    	public static int getSmallest(int[] a, int total) throws Exception{  
		int min = a[0];  
	       for (int i = 0; i < a.length; i++) {  
	    	   if(a[i] > 100)
	    	   {
	    		   throw new Exception("Exceed 100");
	    	   }
	            //Compare elements of array with min  
	           if(a[i] <min)  
	               min = a[i];  
	        }  
		       return min;  
		}  
	public static int[] stringToIntArray(String[] splited) {

		      int size = splited.length;
		      int [] arr = new int [size];
		      for(int i=0; i<size; i++) {
		         arr[i] = Integer.parseInt(splited[i]);
		      }
		      return arr;
		
	}
}
