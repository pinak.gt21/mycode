from flask import Flask, render_template, request,jsonify
from flask_cors import CORS,cross_origin
import matplotlib.pyplot as plt
import pandas as pd
import array as arr
import os
import datetime
import requests
from markupsafe import Markup
from plotly.offline import plot
import plotly.graph_objs as go
from bs4 import BeautifulSoup as bs
from urllib.request import urlopen as uReq
import matplotlib.pyplot as plt
app = Flask(__name__)

@app.route('/',methods=['GET'])  # route to display the home page
@cross_origin()
def homePage():
    return render_template("results.html")

@app.route('/review',methods=['POST','GET']) # route to show the review comments in a web UI
@cross_origin()
def index():
    if request.method == 'POST':
        try:
            #df = pd.read_excel(
            #    r'C:/Pinak/Poc_git_code/poc/plsql_poc_svn/poc_delta_data/Env_data_diff/pmf_data_analysis/Pr02.xlsx')
            df = pd.read_excel(r'Pr02.xlsx')
            #Promocust = pd.read_excel(
            #    r'C:/Pinak/Poc_git_code/poc/plsql_poc_svn/poc_delta_data/Env_data_diff/pmf_data_analysis/All_Promotion_Volumes_04_05_20_trial.xlsx')
            Promocust = pd.read_excel(r'All_Promotion_Volumes_04_05_20_trial.xlsx')
            Promocust.fillna(0, inplace=True)
            Promocust['SCODE'] = Promocust['PART_NUM']

            Promocust.drop(['PART_NUM'], axis=1, inplace=True)
            Promodetails = pd.merge(df, Promocust, on=['SCODE'], how='inner')

            #Promodetails.columns
            Promodetails['Total_Customer'] = Promodetails['TOTAL_ACTIVE'] + Promodetails['TOTAL_INACTIVE']

            Promodetails['CREATION_DATE'] = pd.to_datetime(Promodetails['CREATION_DATE'],
                                                           format='%d-%b-%y %I.%M.%S.%f %p')
            Promodetails['LAST_UPDATE_DATE'] = pd.to_datetime(Promodetails['LAST_UPDATE_DATE'],
                                                              format='%d-%b-%y %I.%M.%S.%f %p')

            Promodetails['CREATION_MONTH'] = Promodetails['CREATION_DATE'].dt.strftime('%Y-%m')
            Promodetails['CREATION_MONTH'].unique()

            Promotion = Promodetails.groupby('CREATION_MONTH')['SCODE'].agg('count')
            Customer = Promodetails.groupby('CREATION_MONTH')['TOTAL_INACTIVE'].agg('sum')
            CustomerActive = Promodetails.groupby('CREATION_MONTH')['TOTAL_ACTIVE'].agg('sum')
            Customer = Customer / 10000
            CustomerActive = CustomerActive / 10000
            bars = []
            bars.append(go.Bar(x=Promotion.index, y=Promotion, name='Promotion Creation Rate'))
            bars.append(go.Bar(x=Customer.index, y=Customer, name='Inactive Customer * 10000'))
            bars.append(go.Bar(x=CustomerActive.index, y=CustomerActive, name='Active Customer * 10000'))
            fig = go.Figure(data=bars)
            fig.update_layout(barmode='group')
            #fig.show()
            #div = fig.to_html(full_html=False)

            divone = plot(fig, output_type='div', include_plotlyjs=False)

            ProductCustomerActive = Promodetails.groupby('STRENGTH')['TOTAL_ACTIVE'].agg('sum')
            ProductCustomerInactive = Promodetails.groupby('STRENGTH')['TOTAL_INACTIVE'].agg('sum')
            bars = []
            bars.append(go.Bar(x=ProductCustomerActive.index, y=ProductCustomerActive, name='Active Customer'))
            bars.append(go.Bar(x=ProductCustomerInactive.index, y=ProductCustomerInactive, name='Inactive of Customer'))
            fig = go.Figure(data=bars)
            fig.update_layout(barmode='group')
            divtwo = plot(fig, output_type='div', include_plotlyjs=False)
            #fig.show()
            Promodetails['MONTH'] = Promodetails['CREATION_DATE'].dt.strftime('%m')
            Promodetails['MONTHTEXT'] = Promodetails['CREATION_DATE'].dt.strftime('%B')
            PatternCustomerActive = Promodetails.groupby(['MONTHTEXT', 'STRENGTH'])['TOTAL_ACTIVE'].agg('sum')
            #PatternCustomerActive.dtype
            PatternCustomerDf = pd.DataFrame(PatternCustomerActive)

            PatternCustomerDf.reset_index(inplace=True)
            bars = []
            for i in PatternCustomerDf['STRENGTH'].unique():
                #PatternCustomerDf[PatternCustomerDf['STRENGTH'] == i]

                bars.append(go.Bar(x=PatternCustomerDf[PatternCustomerDf['STRENGTH'] == i].MONTHTEXT, y=PatternCustomerDf[PatternCustomerDf['STRENGTH'] == i].TOTAL_ACTIVE, name=i))
            fig = go.Figure(data=bars)
            divthree = plot(fig, output_type='div', include_plotlyjs=False)
            divfinal = Markup(divone) + Markup(divtwo)+ Markup(divthree)

            return render_template('results.html', plotly_graph=divfinal)
        except Exception as e:
            print('The Exception message is: ',e)
            return 'something is wrong'
    # return render_template('results.html')

    else:
        return render_template('results.html')

if __name__ == "__main__":
    #app.run(host='127.0.0.1', port=8001, debug=True)
	app.run(debug=True)