import { Injectable } from '@angular/core';
import { HttpRequest ,HttpClient } from '@angular/common/http';


// RequestMethod,RequestOptions, Request, Http

//import { OptionsType  } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MovieService {
  constructor(private http: HttpClient) {
  }
async get(url: string) {
    return await this.http.get(url);
  }
async request(url: string) {
  //  const url = `${config.api.baseUrl}${url}${config.api.apiKey}`


    return await this.http.get(url).toPromise();
  }
}