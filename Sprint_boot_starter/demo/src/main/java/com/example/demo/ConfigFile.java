package com.example.demo;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;


@Configuration
@ConfigurationProperties(prefix = "prop")
public class ConfigFile {

/*	ConfigurableApplicationContext applicationContext = new SpringApplicationBuilder(DemoApplication.class)
			.properties("spring.config.name:application,conf",
					"spring.config.location:classpath:/external/properties/,classpath:/com/roufid/tutorial/configuration/")
			.build().run(); */


	private String url;
	public String getUrl() {
		System.out.println("Details "+url);
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}


	
}
