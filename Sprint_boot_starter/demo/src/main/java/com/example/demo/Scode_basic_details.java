package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Entity;

@Entity
public class Scode_basic_details {
	
	  @Id
	  @GeneratedValue(strategy = GenerationType.SEQUENCE)
	  private Integer id;
//	  @GeneratedValue(generator="system-uuid")
//	  @GenericGenerator(name="system-uuid", strategy = "uuid")
	 
	  
//	  @Column(name = "scode", nullable = true)
      private String scode;
	  
	  
	  private String description;
	  private String icc;
	
	  protected Scode_basic_details() {}
	/*  public Scode_basic_details(String description) { //String scode , 
		  //this.scode = scode;
		    this.description = description;
		    this.icc = icc;

		  }*/
	  

	  public Scode_basic_details(String scode,String icc,String description) { //String scode , 
		  //this.scode = scode;
		  this.scode = scode;
		  this.icc = icc;
		    this.description = description;
		   
		   

		  }
	  
	  
	  @Override
	  public String toString() {
	    return String.format(
	        "Product [scode=%s, icc='%s', description='%s']",
	        scode, icc,description);
	  }
	  


	  public String getDescription() {
	    return description;
	  }
	  
	  public String getIcc() {
		    return icc;
		  }
	  
	 public String getScode() {
		    return scode;
		  }


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	

	
}
