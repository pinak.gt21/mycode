package com.example.demo;
import java.util.List;



import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface ProductRepository  extends CrudRepository<Scode_basic_details, Long> {
	

 	List<Scode_basic_details> findByScode(String scode);
	List<Scode_basic_details> findAll();

	Scode_basic_details findById(long id);
   // String save(Scode_basic_details scodedetails);
	

	
}
