package com.example.demo;

import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class HelloController {

	@Autowired
	private ProductRepository repository;

	@Autowired
	private ConfigFile config;

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	
/*	   @CrossOrigin(origins = "http://localhost:4200")
	   @RequestMapping("/data") public String proddata() throws Exception {
	 //List<Scode_basic_details> listscode = new ArrayList<Scode_basic_details>();
	 List<Scode_basic_details> listscode = repository.findByScode("S001");
	  ObjectMapper mapper = new ObjectMapper(); String jsonInString =
	 mapper.writeValueAsString(listscode); return jsonInString;
	  
	  } */
	   
	   
	   @CrossOrigin(origins = "http://localhost:4200")
	   @RequestMapping("/data") public String prodAlldata() throws Exception {
	 //List<Scode_basic_details> listscode = new ArrayList<Scode_basic_details>();
	 List<Scode_basic_details> listscode = repository.findAll();
	  ObjectMapper mapper = new ObjectMapper(); String jsonInString =
	 mapper.writeValueAsString(listscode); return jsonInString;
	  
	  }
    
   	   @CrossOrigin(origins = "http://localhost:4200")
	   @RequestMapping(value ="/create", method = RequestMethod.POST) 
	   @ResponseBody
	   public String prodCreate(@RequestParam Map<String,String> allParams ) //,@RequestParam(name = "scode") String scode,@RequestParam(name = "icc") String icc,@RequestParam(name = "description") String description) 
			   throws Exception {
       
   		System.out.println("Parameters are " + allParams.entrySet());
   		   System.out.println("Entering prodCreate");
   		   
   		
      Scode_basic_details scodedetails = new Scode_basic_details(allParams.get("scode"),allParams.get("icc"),allParams.get("description"));
 //      System.out.println("Scode "+scode);
   //    System.out.println("icc "+icc);
 //      System.out.println("description "+description);
       scodedetails.toString();
      repository.save(scodedetails);
      //Scode_basic_details savedetails =  repository.save(scodedetails);
       String statusreturn  = "{\"status\":\"true\",\"message\":\"success\"}";
       return statusreturn;
	  }
	   
	   
	@CrossOrigin(origins = "http://localhost:4200/welcome")
	@RequestMapping("/test")
	public String proddata(@RequestParam(name = "X-Requested-With") String requestWith,@RequestParam(name = "authCode") String authCode,@RequestParam(name = "REDIRECT_URI") String REDIRECT_URI ) throws Exception {

		if (requestWith == null) {
			// Without the `X-Requested-With` header, this request could be forged. Aborts.
		//	break;
		}

		// Set path to the Web application client_secret_*.json file you downloaded from
		// the
		// Google API Console: https://console.developers.google.com/apis/credentials
		// You can also find your Web application client ID and client secret from the
		// console and specify them directly when you create the
		// GoogleAuthorizationCodeTokenRequest
		// object.
		//client-id: 191350097820-bompejk5mlp0ttdeb0rt1aub91dh26up.apps.googleusercontent.com
        // client-secret: gJU2mDIENS4hieFLcz1JqIjY
		String CLIENT_SECRET_FILE = "src/main/resources/client_secret.json";

		// Exchange auth code for access token
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JacksonFactory.getDefaultInstance(),
				new FileReader(CLIENT_SECRET_FILE));

		GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(new NetHttpTransport(),
				JacksonFactory.getDefaultInstance(), "https://oauth2.googleapis.com/token",
				clientSecrets.getDetails().getClientId(), clientSecrets.getDetails().getClientSecret(), authCode,
				REDIRECT_URI) // Specify the same redirect URI that you use with your web
								// app. If you don't have a web version of your app, you can
								// specify an empty string.
						.execute();

		String accessToken = tokenResponse.getAccessToken();

		// Use access token to call API
		GoogleCredential credential = new GoogleCredential().setAccessToken(accessToken);
		Drive drive = new Drive.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance(), credential)
				.setApplicationName("Auth Code Exchange Demo").build();
		com.google.api.services.drive.model.File file = drive.files().get("appfolder").execute();

		// Get profile info from ID token
		GoogleIdToken idToken = tokenResponse.parseIdToken();
		GoogleIdToken.Payload payload = idToken.getPayload();
		String userId = payload.getSubject(); // Use this value as a key to identify a user.
		String email = payload.getEmail();
		boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
		String name = (String) payload.get("name");
		String pictureUrl = (String) payload.get("picture");
		String locale = (String) payload.get("locale");
		String familyName = (String) payload.get("family_name");
		String givenName = (String) payload.get("given_name");
		List<Scode_basic_details> listscode = repository.findAll();
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(listscode);
		return jsonInString;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/hello")
	public String helldata() throws Exception {
		// List<Scode_basic_details> listscode = new ArrayList<Scode_basic_details>();
		return "Hello from Spring Boot!";

	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/config")
	public String configdata() {

		// List<Scode_basic_details> listscode = new ArrayList<Scode_basic_details>();
		// return "abc";
		return config.getUrl();

	}

}
