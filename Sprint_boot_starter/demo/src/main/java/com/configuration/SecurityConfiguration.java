/*package com.configuration;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;

import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;


//import com.auth0.spring.security.api.JwtWebSecurityConfigurer;
//@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
//	@Value(value = "${auth0.apiAudience}")
//	private String apiAudience;
//	@Value(value = "${auth0.issuer}")
//	private String issuer;
//@Autowired
//private Oauth2AuthenticationSuccessHandler oauthSuccessHandler;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.formLogin().loginPage("/")
			.failureUrl("/login-error")
			.defaultSuccessUrl("/data",true)

			.and()
			.oauth2Login()
			.and()
		.rememberMe()
		
			.and().oauth2Login()
			.loginPage("/")
		//	.successHandler(successHandler)
			.and()
		.authorizeRequests()
			.mvcMatchers("/register","/login","/","/login-error",
					"/login-verified").permitAll()
			.mvcMatchers("/portfolio/**").hasRole("USER")
			.mvcMatchers("/support/**").hasAnyRole("USER","ADMIN")
			.mvcMatchers("/support/admin/**").access("isFullyAuthenticated() and hasRole('ADMIN')")
			.mvcMatchers("/api/users").hasRole("ADMIN")
			.mvcMatchers("/api/users/{username}/portfolio")
				.access("@isPortfolioOwnerOrAdmin.check(#username)")
			.anyRequest().denyAll();
		
		//http.cors().and().csrf().disable().anonymous().disable().authorizeRequests().antMatchers("/oauth/token").permitAll();
	//	http.cors().and().authorizeRequests().antMatchers("/oauth/token/").permitAll();
        
//	    JwtWebSecurityConfigurer
   //     .forRS256(apiAudience, issuer)
   //     .configure(http)
   //    .authorizeRequests()
      //  .antMatchers(HttpMethod.POST, "/api/v1/bikes").permitAll()
   //     .antMatchers(HttpMethod.GET, "/data").hasAuthority("view:registrations")
   //     .antMatchers(HttpMethod.GET, "/data/**").hasAuthority("view:registration");
        //.anyRequest().authenticated();
		
		http.csrf().disable()
        .authorizeRequests()
        .antMatchers("/api1/**").permitAll()
        .antMatchers("/api2/**").permitAll()
        .antMatchers("/api3/**").permitAll();
	} 
	
	

	
	
/*	@Bean
	public CorsFilter corsFilter() {
	    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    CorsConfiguration config = new CorsConfiguration();
	    config.addAllowedOrigin("*");
	    config.addAllowedHeader("*");
	    config.addAllowedMethod("*");
	    config.addExposedHeader("Authorization");
	    config.addExposedHeader("Content-Type");
	    source.registerCorsConfiguration("/**", config);
	    return new CorsFilter(source);
	}*/

	


//}
