DROP TABLE IF EXISTS Scode_basic_details;

CREATE TABLE Scode_basic_details (
  ID bigint auto_increment,
  SCODE VARCHAR(250)  NOT NULL,
  DESCRIPTION VARCHAR(250) NOT NULL,
  ICC VARCHAR(250) NOT NULL
);

INSERT INTO Scode_basic_details (SCODE, DESCRIPTION , ICC) VALUES
  ( 'S001', 'PSTN','Product'),
  ( 'S002', 'Mobile Props','Promotion');