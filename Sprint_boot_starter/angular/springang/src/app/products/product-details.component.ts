import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IProduct } from './product';
import { ProductService } from './product-services';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  errorMessage = '';
  pageTitle = 'Product Details';
  product: IProduct [] = [];
  client_id : string = '191350097820-bompejk5mlp0ttdeb0rt1aub91dh26up.apps.googleusercontent.com';
  redirect_uri : string = 'http://localhost:8081/login/oauth2/code/google';
  response_type : string = 'token';
  scope : string = 'https://www.googleapis.com/auth/drive.metadata.readonly';
  include_granted_scopes : string = 'true';

  state : string = 'pass-through value';

  constructor(private route: ActivatedRoute,
    private router: Router,private productService: ProductService) { }

  ngOnInit(): void {
    this.getProduct();
   // this.getLogin();
  }


  getProduct() {
    this.productService.getProductParam().subscribe({
      next: product => this.product = product,
      error: err => this.errorMessage = err
    });
  }


  getLogin() {
    this.productService.getLoginParam(this.client_id,this.redirect_uri,this.response_type,this.scope,this.include_granted_scopes,this.state).subscribe(data => {


    })
  }

}
