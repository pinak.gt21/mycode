import { HttpClient, HttpErrorResponse , HttpHeaders } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { IProduct } from './product';
import { catchError, tap, map } from 'rxjs/operators';
import { Config } from './config';


interface myData {
    status: string,
    message: string
  }
  
@Injectable({
    providedIn: 'root'
  })
  

export class ProductService {

    public hosturl = 'https://accounts.google.com/o/oauth2/v2/auth';
    config = new Config;
    constructor(private http: HttpClient) { }

    createUrl: string = 'http://localhost:8081/create';
    paramUrl : string = 'http://localhost:8081/data';




    getProductParam(): Observable<IProduct[]> {
        //this.setEndPointUrl()

         console.log('Hitting recon '+this.paramUrl)
          return this.http.get<IProduct[]>(this.paramUrl).pipe(
              tap(data => console.log('All: ' + JSON.stringify(data))),
              catchError(this.handleError)
            );
        }

    getLoginParam(client_id,redirect_uri,response_type,scope,include_granted_scopes,state){

            //this.setEndPointUrl()
             console.log('Hitting recon '+this.hosturl)
             return this.http.post<myData>(this.hosturl, {
                client_id,
                redirect_uri,
                response_type,
                scope,
                include_granted_scopes,
                state,
              })
            }


            getLoginHeaderParam(client_id,redirect_uri,response_type,scope,include_granted_scopes,state){

              //this.setEndPointUrl()
               console.log('Hitting recon '+this.hosturl)
               return this.http.post<myData>(this.hosturl, {
                  client_id,
                  redirect_uri,
                  response_type,
                  scope,
                  include_granted_scopes,
                  state,
                })
              }
    
     createProductDetails(scode, icc,description) {
                // post these details to API server return user info if correct
                const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')


                console.log('calling createProductDetails')
                console.log('Hit login url '+this.createUrl)
                console.log("scode "+scode)
                console.log("icc "+icc)
                console.log("description "+description)

                let data = new URLSearchParams();
                data.append('scode', scode);
                data.append('icc', icc);
                data.append('description', description);
                let body = data.toString()
                console.log("Body" + body)
                return this.http.post<myData>(this.createUrl, body, {headers:myheader})
              }   
  private handleError(err: HttpErrorResponse) {

    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
    
}

