import { HttpHeaders } from '@angular/common/http';

export class Config {
    url: string = 'http://localhost:8081';
    httpOptions: any = {
        headers: new HttpHeaders({
           'Access-Control-Allow-Origin': '*',
           'Authorization':'authkey',
           'userid':'1'
        })
    }
}