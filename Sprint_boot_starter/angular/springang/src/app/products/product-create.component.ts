import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from './product-services';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {

  pageTitle = 'Product Create';

  errorMessage = '';


  constructor(private route: ActivatedRoute,private productservice : ProductService, 
    private router: Router) { }

  ngOnInit(): void {
  }


  createProduct(event) {
    event.preventDefault()
    const target = event.target
    const scode = target.querySelector('#Scode').value
    const icc = target.querySelector('#Icc').value
    const description = target.querySelector('#Description').value

    this.productservice.createProductDetails(scode, icc,description).subscribe(data => {
      if(data.status == 'true') {
        console.log(data)
        console.log(data.status)



      } else {
        console.log('return '+data.toString())
        window.alert(data.message)
      }
    })
   
  }

}
