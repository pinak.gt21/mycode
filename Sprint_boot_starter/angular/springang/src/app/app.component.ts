import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { filter } from 'rxjs/operators';
import { authCodeFlowConfig } from './auth.config';

@Component({
  selector: 'app-root',
  template: `  
  <nav class='navbar navbar-expand navbar-light bg-light'>
  <a class='navbar-brand'>{{pageTitle}}</a>
 
   <ul class='nav nav-pills'>
   <li><a class='nav-link' routerLinkActive='active' [routerLink]="['/welcome']">Home</a></li>
   <li><a class='nav-link' routerLinkActive='active' [routerLink]="['/create']">Create Product</a></li>
    <li><a class='nav-link' href="" (click)="logout()">Logout</a></li>
    </ul>
</nav>
<div class='container'>
<router-outlet></router-outlet>
</div>
`,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  pageTitle = 'Product Management';
  constructor(private route: ActivatedRoute,
    private router: Router) {
      console.log("Enter app")
    /*  this.oauthService.configure(authCodeFlowConfig);
      this.oauthService.loadDiscoveryDocumentAndLogin();
      this.oauthService.events
      .pipe(filter(e => e.type === 'token_received'))
      .subscribe(_ => this.oauthService.loadUserProfile()); */
     }

  logout() {

    sessionStorage.removeItem('loggedIn')
    sessionStorage.removeItem('logid')
    sessionStorage.removeItem('logpass')
    console.log('remove session Object')
    console.log(sessionStorage.getItem('logid'))
    this.router.navigate(['home'])
    
  }


 /* get userName(): string {
    const claims = this.oauthService.getIdentityClaims();
    if (!claims) return null;
    return claims['given_name'];
  }

  get idToken(): string {
    return this.oauthService.getIdToken();
  }

  get accessToken(): string {
    return this.oauthService.getAccessToken();
  }

  refresh() {
    this.oauthService.refreshToken();
  } */
}
