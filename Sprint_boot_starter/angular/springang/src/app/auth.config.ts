import { AuthConfig } from 'angular-oauth2-oidc';

export const authCodeFlowConfig: AuthConfig = {
  issuer: 'https://accounts.google.com/',
  redirectUri: 'http://localhost:8081/login/oauth2/code/google',
  clientId: '191350097820-bompejk5mlp0ttdeb0rt1aub91dh26up.apps.googleusercontent.com',
  responseType: 'token',
  scope: 'https://www.googleapis.com/auth/drive.metadata.readonly',
  showDebugInformation: true,
  timeoutFactor: 0.01
};