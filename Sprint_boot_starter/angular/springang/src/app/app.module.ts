import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { ProductDetailsComponent } from './products/product-details.component';
import { APP_INITIALIZER } from '@angular/core';
import { ProductCreateComponent } from './products/product-create.component';



@NgModule({
  declarations: [
    AppComponent,
    ProductDetailsComponent,
    ProductCreateComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot([
      { path: 'welcome', component: ProductDetailsComponent },
      { path: 'create', component: ProductCreateComponent }
   
   ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
