# -*- coding: utf-8 -*-
"""
Created on Wed May  6 22:46:15 2020

@author: PG
"""

import pandas as pd
import numpy as np


    
TrainData = pd.read_csv('C:/Pinak/Mygit/Titanic/titanic/train.csv')
TestData = pd.read_csv('C:/Pinak/Mygit/Titanic/titanic/test.csv')
len(TrainData)
TrainData.columns

TrainData.isnull().sum()


def bar_chart(feature):
    survived = TrainData[TrainData['Survived']==1][feature].value_counts()
    dead = TrainData[TrainData['Survived']==0][feature].value_counts()
    df = pd.DataFrame([survived,dead])
    df.index = ['Survived','Dead']
    df.plot(kind='bar',stacked=True, figsize=(10,5))
    

train_test_data = [TestData,TrainData]
for dataset in train_test_data:
    dataset['Title'] = dataset['Name'].str.extract('([A-Za-z]+)\.',expand=False)

TrainData['Title']


title_mapping = {"Mr": 0, "Miss": 1, "Mrs": 2, 
                 "Master": 3, "Dr": 3, "Rev": 3, "Col": 3, "Major": 3, "Mlle": 3,"Countess": 3,
                 "Ms": 3, "Lady": 3, "Jonkheer": 3, "Don": 3, "Dona" : 3, "Mme": 3,"Capt": 3,"Sir": 3 }
for dataset in train_test_data:
    dataset['Title'] = dataset['Title'].map(title_mapping)


    
    
TrainData['Age'].fillna(TrainData.groupby("Title")["Age"].transform("median"),inplace=True)

#TrainData['Age']=TrainData['Age'].fillna(TrainData['Age'].mean())

#drop embarked no use as does not matter where from someone boardeed the ship
#Cabin has more than 50% data as nan so column will be dropped 
#Fare has nothing to do with survival so dropped 
#TrainData.drop(['Embarked'],axis=1,inplace=True)

bar_chart('Embarked')


for dataset in train_test_data:
    dataset['Embarked'] = dataset['Embarked'].fillna('S')
    
embarked_mapping = {"S": 0, "C": 1, "Q": 2}
for dataset in train_test_data:
    dataset['Embarked'] = dataset['Embarked'].map(embarked_mapping)
    
    
TrainData.drop(['Cabin'],axis=1,inplace=True)
TrainData.drop(['Fare'],axis=1,inplace=True)
from plotly.offline import plot
# Need to understand if Sibsp and parch has any relation with survival
import plotly.graph_objs as go
bars = []
bars.append(go.Bar(x = TrainData['Survived'],y=TrainData['SibSp']))
bars.append(go.Bar(x = TrainData['Survived'],y=TrainData['Parch']))

fig = go.Figure(data=bars)
fig.update_layout(yaxis_type="log")
plot(fig)

# No relation 

# TrainData.drop(['SibSp'],axis=1,inplace=True)
# TrainData.drop(['Parch'],axis=1,inplace=True)
TrainData.drop(['PassengerId'],axis=1,inplace=True)
TrainData.drop(['Ticket'],axis=1,inplace=True)

TrainData.drop(['Name'],axis=1,inplace=True)

TrainData['Pclass'].unique() 
TrainData['Sex'].unique() 


mapping = {k: v for v, k in enumerate(TrainData.Sex.unique())}
TrainData['Sex'] = TrainData.Sex.map(mapping)

y_train=TrainData['Survived']
X_train=TrainData.drop(['Survived'],axis=1)


# Test Data parsing 


len(TestData)
TestData.columns

TestData.isnull().sum()

#TestData['Age']=TestData['Age'].fillna(TestData['Age'].mean())
#train_test_data = [TestData,TrainData]
TestData['Age'].fillna(TestData.groupby("Title")["Age"].transform("median"),inplace=True)
#drop embarked no use as does not matter where from someone boardeed the ship
#Cabin has more than 50% data as nan so column will be dropped 
#Fare has nothing to do with survival so dropped 
#TestData.drop(['Embarked'],axis=1,inplace=True)
TestData.drop(['Cabin'],axis=1,inplace=True)
TestData.drop(['Fare'],axis=1,inplace=True)


# No relation 

# TestData.drop(['SibSp'],axis=1,inplace=True)
# TestData.drop(['Parch'],axis=1,inplace=True)
TestData.drop(['PassengerId'],axis=1,inplace=True)
TestData.drop(['Ticket'],axis=1,inplace=True)

TestData.drop(['Name'],axis=1,inplace=True)

TestData['Pclass'].unique() 
TestData['Sex'].unique() 

TestData['Sex'] = TestData.Sex.map(mapping)

#y_train=TrainData['Survived']
X_Test=TestData




from keras import backend as K
def root_mean_squared_error(y_true, y_pred):
        return K.sqrt(K.mean(K.square(y_pred - y_true)))

# Importing the Keras libraries and packages
import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LeakyReLU,PReLU,ELU
from keras.layers import Dropout


# Initialising the ANN
classifier = Sequential()

# Adding the input layer and the first hidden layer
classifier.add(Dense(output_dim = 50,activation='relu',init = 'RandomNormal',input_dim = 7))

# Adding the second hidden layer
classifier.add(Dense(output_dim = 50,init = 'RandomNormal',activation='relu'))
classifier.add(Dropout(0.5))
# Adding the second hidden layer
classifier.add(Dense(output_dim = 50,init = 'RandomNormal',activation='relu'))
classifier.add(Dropout(0.5))
# Adding the second hidden layer
classifier.add(Dense(output_dim = 50,init = 'RandomNormal',activation='relu'))


# Adding the fourth hidden layer
classifier.add(Dense(output_dim = 25,init = 'RandomNormal',activation='softmax'))
# Adding the output layer
classifier.add(Dense(1, activation='sigmoid'))

# Compiling the ANN
classifier.compile(loss=root_mean_squared_error, optimizer='Adamax')

# Fitting the ANN to the Training set
model_history=classifier.fit(X_train.values, y_train.values,validation_split=1, batch_size = 5, nb_epoch = 1000)
#test_df['MSZoning']=test_df['MSZoning'].fillna(test_df['MSZoning'].mode()[0])


ann_pred=classifier.predict(X_Test.values)

y_test = pd.read_csv('C:/Pinak/Mygit/Titanic/titanic/gender_submission.csv')
X_Test['Survived'] = ann_pred.round()
submissiondata = pd.read_csv('C:/Pinak/Mygit/Titanic/titanic/test.csv')
X_Test['PassengerId'] = submissiondata['PassengerId']
final = X_Test[['PassengerId', 'Survived']]
final.to_csv(r'C:/Pinak/Mygit/Titanic/titanic/final_v1.csv' ,index = None, header=True)
from sklearn.metrics import accuracy_score
print(accuracy_score(final.Survived, y_test.Survived))