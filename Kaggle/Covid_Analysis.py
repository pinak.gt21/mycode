
import pandas as pd
import requests
import chart_studio.plotly as py
from bs4 import BeautifulSoup as bs
df = pd.DataFrame()
#url = "http://shortsqueeze.com/?symbol=" + stock + "&submit=Short+Quote%E2%84%A2"
url = "https://www.worldometers.info/coronavirus/"
repeat_times = 1
downloadFailed = True
header = {
  "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
  "X-Requested-With": "XMLHttpRequest"
}

stock = "India"
for _ in range(repeat_times): 
    try:
        response = requests.get(url,header, timeout=1000 )
        downloadFailed = False
        break
    except Exception as e:
        print ("exception in get url:" , str(e))
        continue

if downloadFailed:
    print("Download Fail")


#bs_html = bs(response.text, "html.parser") # parsing the  page as HTML
soup = bs(response.text,'lxml')
table = soup.find("table", {"id" : "main_table_countries_today"})
df = pd.read_html(str(table))
# table_rows = table.find_all('tr')
# for tr in table_rows:
#     td = tr.find_all('td')
#     row = [i.text.strip() for i in td]
#     print(row)

                
                
try:    
    today_df = pd.read_html(response.text,header=[0], index_col=0)[0].iloc[:-1]
    yesterday_df = pd.read_html(response.text, header=[0], index_col=0)[1].iloc[:-1]
except Exception as e:
    print ("exception in parse website:", str(e))
    
    
    

#Data Kaggle collected 
    
covid = pd.read_csv('C:/Pinak/Mygit/novel-corona-virus-2019-dataset/covid_19_data.csv')
COVID19_line_list_data = pd.read_csv('C:/Pinak/Mygit/novel-corona-virus-2019-dataset/COVID19_line_list_data.csv')
COVID19_open_line_list = pd.read_csv('C:/Pinak/Mygit/novel-corona-virus-2019-dataset/COVID19_open_line_list.csv')
time_series_covid_19_confirmed = pd.read_csv('C:/Pinak/Mygit/novel-corona-virus-2019-dataset/time_series_covid_19_confirmed.csv')
time_series_covid_19_confirmed_US = pd.read_csv('C:/Pinak/Mygit/novel-corona-virus-2019-dataset/time_series_covid_19_confirmed_US.csv')
time_series_covid_19_deaths= pd.read_csv('C:/Pinak/Mygit/novel-corona-virus-2019-dataset/time_series_covid_19_deaths.csv')
time_series_covid_19_deaths_US = pd.read_csv('C:/Pinak/Mygit/novel-corona-virus-2019-dataset/time_series_covid_19_deaths_US.csv')
time_series_covid_19_recovered = pd.read_csv('C:/Pinak/Mygit/novel-corona-virus-2019-dataset/time_series_covid_19_recovered.csv')




import matplotlib.pyplot as plt
import seaborn as sns 
import statsmodels as sm
import folium as fl
from pathlib import Path
from sklearn.impute import SimpleImputer
import plotly.offline as py
import plotly.express as px
import cufflinks as cf


covid.head()
mis = covid.isnull().sum()
mis[mis>0]

imputer = SimpleImputer(strategy='constant')#here I use constant because I cannot put another Province/State
#that we do not know or that does not correspond to his country/region  
impute_covid = pd.DataFrame(imputer.fit_transform(covid), columns=covid.columns)
impute_covid.head()


#convert ObservationDate and Last Update object to datetime
#convert confirmed, recovered, death to numeric
impute_covid['ObservationDate'] = pd.to_datetime(impute_covid['ObservationDate'])
impute_covid['Last Update'] = pd.to_datetime(impute_covid['Last Update'])
impute_covid['Confirmed'] = pd.to_numeric(impute_covid['Confirmed'], errors='coerce')
impute_covid['Recovered'] = pd.to_numeric(impute_covid['Recovered'], errors='coerce')
impute_covid['Deaths'] = pd.to_numeric(impute_covid['Deaths'], errors='coerce')

impute_covid.info()


# we compute the active_confirmed
impute_covid['active_confirmed'] = impute_covid['Confirmed'].values - \
(impute_covid['Deaths'].values+impute_covid['Recovered'].values)

start_date = impute_covid.ObservationDate.min()
end_date = impute_covid.ObservationDate.max()
print('Novel Covid-19 information:\n 1. Start date = {}\n 2. End date = {}'.format(start_date, end_date))


worldwide = impute_covid[impute_covid['ObservationDate'] == end_date]



nb_country = len(worldwide['Country/Region'].value_counts()) # number country
worldwide['Country/Region'].value_counts()

world = worldwide.groupby('Country/Region').sum()
world = world.sort_values(by=['Confirmed'], ascending=False)
world.style.background_gradient(cmap='viridis')



print('================ Worldwide report ===============================')
print('== Information to {} on novel COVID-19 =========\n'.format(end_date))
print('Tota confirmed: {}\nTotal Deaths: {}\nTotal Recovered: {}\nTotal active confirmed: {}\n\
Total country Recorded: {} \n'.format(\
worldwide.Confirmed.sum(), worldwide.Deaths.sum(), worldwide.Recovered.sum(), worldwide.active_confirmed.sum(),\
                                     nb_country))
print('==================================================================')


from plotly.offline import plot
# import plotly as py
# import plotly.tools as tls
# import cufflinks as cf
# import pandas as pd
import numpy as np
# import seaborn as sns
# import matplotlib.pyplot as plt
# import plotly.graph_objs as go
# %matplotlib inline

# import plotly.io as pio

# pio.renderers.default='svg'

import plotly.graph_objs as go

fig = go.Figure(data=[{'type': 'scatter', 'y': world.Confirmed,'x':world.index}])
fig.update_layout(yaxis_type="log")
fig.show()
fig = go.Figure(data=[{'type': 'scatter', 'y': world.Recovered,'x':world.index}])
fig.update_layout(yaxis_type="log")
fig.show()

fig = go.Figure(data=[{'type': 'scatter', 'y': world.Deaths,'x':world.index}])
fig.update_layout(yaxis_type="log")
fig.show()

fig = go.Figure(data=[{'type': 'scatter', 'y': world.active_confirmed,'x':world.index}])
fig.update_layout(yaxis_type="log")
fig.show()

#plot(fig)

world_table = world.reset_index()
x = world_table[world_table['Country/Region'] == 'France']
big_7 = world_table[world_table['Confirmed'] >= x.iloc[0,1]]


#Special China

china = impute_covid[impute_covid['Country/Region'] == 'Mainland China']

chstar_date = china.ObservationDate.min()



chend_date = china.ObservationDate.max()


print('Novel covid-19 China:\n start date = {}\n end date = {}'.format(chstar_date, chend_date))


lastChina = china[china['ObservationDate'] == chend_date]
lastChina.head(2)


print('================ China report ===================================')
print('== Information to {} on novel COVID-19 =========\n'.format(chend_date))
print('Tota confirmed: {}\nTotal Deaths: {}\nTotal Recovered: {}\nTotal active confirmed: {}\n'.format(\
lastChina.Confirmed.sum(), lastChina.Deaths.sum(), lastChina.Recovered.sum(), lastChina.active_confirmed.sum()))
print('==================================================================')



province = lastChina.groupby('Province/State').sum()
province = province.sort_values(by=['Confirmed'], ascending=False)

plt.bar(province.index,province['Confirmed'], color = 'r', width = 0.25)
plt.bar(province.index,province['Deaths'], color = 'g', width = 0.25)
plt.bar(province.index,province['Recovered'], color = 'b', width = 0.25)
plt.bar(province.index,province['active_confirmed'], color = 'y', width = 0.25)
plt.show()


bars = []
bars.append(go.Bar(x = province.index,y=province['Confirmed']))
bars.append(go.Bar(x = province.index,y=province['Deaths']))
bars.append(go.Bar(x = province.index,y=province['Recovered']))
bars.append(go.Bar(x = province.index,y=province['active_confirmed']))
fig = go.Figure(data=bars)
fig.update_layout(yaxis_type="log")
plot(fig)

conf_china = china.groupby('ObservationDate')['Confirmed'].agg('sum')
rec_china = china.groupby('ObservationDate')['Recovered'].agg('sum')
dea_china = china.groupby('ObservationDate')['Deaths'].agg('sum')
ac_china = china.groupby('ObservationDate')['active_confirmed'].agg('sum')



fig = plt.figure(figsize = (20,20))
fig.subplots_adjust(hspace=0.4, wspace=0.125)
dg = [conf_china,rec_china,dea_china,ac_china]
color = ['blue', 'green', 'red', 'yellow']
names = ['Confirmed', 'Recovered', 'Death', 'Active Confirmed']
bars = []
for i in range(1, len(dg)+1):
    ax = fig.add_subplot(2,2,i)
    #dg[i-1].iplot(kind='bar', color = color[i-1], legend=True)
    bars.append(go.Bar(x = dg[i-1].index,y=dg[i-1],name=names[i-1], marker_color=color[i-1]))
    ax.set_ylabel('Total patient')

fig = go.Figure(data=bars)
fig.update_layout(yaxis_type="log")
plot(fig)   


#ROW


rest_world = impute_covid[impute_covid['Country/Region'] != 'Mainland China']
rest_world.head(2)

print('Novel covid-19 ROW:\n start date = {}\n end date = {}'.format(rest_world.ObservationDate.min(),\
                    rest_world.ObservationDate.max()))
    
row = rest_world[rest_world['ObservationDate'] == rest_world.ObservationDate.max()]
    
print('================ ROW report =====================================')
print('== Information to {} on novel COVID-19 =========\n'.format(chend_date))
print('Tota confirmed: {}\nTotal Deaths: {}\nTotal Recovered: {}\nTotal active confirmed: {}\n'.format(\
row.Confirmed.sum(), row.Deaths.sum(), row.Recovered.sum(), row.active_confirmed.sum()))
print('==================================================================')

obs_conf_world = rest_world.groupby('ObservationDate')['Confirmed'].aggregate([np.sum]) # confirmed obs
ac_conf_world = rest_world.groupby('ObservationDate')['active_confirmed'].aggregate([np.sum]) # last upd obs
patient_world_r = rest_world.groupby('ObservationDate')['Recovered'].aggregate([np.sum]) # lifetime 
patient_world_dea = rest_world.groupby('ObservationDate')['Deaths'].aggregate([np.sum]) # lifetime 

obs_conf_world.columns = ['Confirmed']
ac_conf_world.columns = ['active_onfirmed']
patient_world_r.columns = ['Recovered'] 
patient_world_dea.columns = ['Deaths'] 



fig = plt.figure(figsize = (20,20))
fig.subplots_adjust(hspace=0.4, wspace=0.125)
dr = [obs_conf_world, patient_world_r, patient_world_dea, ac_conf_world]
color = ['blue', 'green', 'red', 'blue']
for i in range(1, len(dr)+1):
    ax = fig.add_subplot(2,2,i)
    dr[i-1].plot(ax=ax, kind='bar', color = color[i-1], legend=True)
    ax.set_ylabel('Total patient')



#Comparison China vs ROW
    
#time_obs.iplot(title='novel COVID-19 in the Worldwide', kind='bar')
time_obs = impute_covid.groupby('ObservationDate')['Confirmed'].aggregate([np.sum])
time_obs.columns = ['Confirmed']
g =time_obs.plot(figsize=(20,8), kind='bar', color='black')
conf_china.plot(ax=g, kind='bar', color = 'blue', legend=True)
plt.title('novel COVID-19 in the Worldwide: China(Blue) vs ROW(Black)')
plt.ylabel('Total patient')
    

death_rate = impute_covid.groupby('ObservationDate')['Deaths'].aggregate([np.sum])
recovered_rate = impute_covid.groupby('ObservationDate')['Recovered'].aggregate([np.sum])
activecase_rate = impute_covid.groupby('ObservationDate')['active_confirmed'].aggregate([np.sum])
death_rate.columns = ['Death rate']
recovered_rate.columns = ['Recovered rate']
activecase_rate.columns = ['Active confirmed rate']
h = recovered_rate.plot(figsize=(15.5, 5), colormap='Greens_r', kind='bar', legend=True) 
rec_china.plot(ax=h, color='yellow', legend=True, kind='bar')
plt.title('novel COVID-19 in the Worldwide: China(yellow) vs ROW(green)')
plt.ylabel('Total patient')
    
df = death_rate.plot(figsize=(15.5, 5), colormap='Reds_r', kind='bar', legend=True)
dea_china.plot(ax=df, color='black', kind='bar', legend=True)
plt.title('novel COVID-19 in the Worldwide: China(Black) vs ROW(Red) ')
plt.ylabel('Total patient')

q = activecase_rate.plot(figsize=(15.5, 5),  color='black', kind='bar', legend=True)
ac_china.plot(kind='bar', color='blue', legend=True, ax=q)
plt.title('novel COVID-19 in the Worldwide: China(Blue) vs ROW(Black)')
plt.ylabel('Total patient')    
    


#rate of death fraction  compared to previous date 
p_count = 0
# datelist= death_rate.index.sorted().tolist()
# death_rate.loc[p_start_dat]

dr = pd.DataFrame(columns=['Datetime', 'DeathRate', 'RecoveryRate','ActiveConfirmed'])
from datetime import date, timedelta
p_counter = 0;
rec_fraction = 0;
act_fraction  = 0;
for i in death_rate.index:
    death_fraction = 0
    
    previous_date = i - timedelta(days=1)
    #print(previous_date)
    current_rate = death_rate.iloc[death_rate.index.get_loc(i)][0]
    try:
        prev_death_rate = death_rate.iloc[death_rate.index.get_loc(previous_date)][0]
    except:
        prev_death_rate = 0
    
    if prev_death_rate != 0:
        death_fraction = ((current_rate -prev_death_rate)/prev_death_rate)*100
   
#recover rate     
    recover_rate = recovered_rate.iloc[recovered_rate.index.get_loc(i)][0]
    try:
        prev_rec_rate = recovered_rate.iloc[recovered_rate.index.get_loc(previous_date)][0]
    except:
        prev_rec_rate = 0
    
    if prev_rec_rate != 0:
        rec_fraction = ((recover_rate -prev_rec_rate)/prev_rec_rate)*100

#Active confirmed 
    active_rate = activecase_rate.iloc[activecase_rate.index.get_loc(i)][0]
    try:
        prev_act_rate = activecase_rate.iloc[activecase_rate.index.get_loc(previous_date)][0]
    except:
        prev_act_rate = 0
    
    if prev_act_rate != 0:
        act_fraction = ((active_rate -prev_act_rate)/prev_act_rate)*100
    #print(death_fraction)
    dr.loc[p_counter,'Datetime'] = i
    dr.loc[p_counter,'DeathRate'] = death_fraction
    dr.loc[p_counter,'RecoveryRate'] = rec_fraction
    dr.loc[p_counter,'ActiveConfirmed'] = act_fraction
    p_counter = p_counter +1



bars = []
bars.append(go.Bar(x =dr.Datetime,y=dr['DeathRate'],name='Death Rate'))
bars.append(go.Bar(x = dr.Datetime,y=dr['RecoveryRate'],name='Recovery Rate'))
bars.append(go.Bar(x = dr.Datetime,y=dr['ActiveConfirmed'],name='Active Rate'))

fig = go.Figure(data=bars)
#fig.update_layout(yaxis_type="log")
plot(fig)   

# Lets find the current graph of growth for all this factor together 
    
   

    
    
    
    
    
# import plotly.io as pio
# pio.renderers.default = "browser"
# from plotly.offline import plot
# province.iplot(kind='bar', yTitle='Total patient',logy=True,
#                title='China Province  with novel covid-19')

# province.iplot(kind='bar', yTitle='Total patient',logy=True,
#                title='China Province  with novel covid-19')
# colors = ['lightslategray',] * 5
# y_plot_data = province.values.tolist()
# fig = go.Figure(data=[go.Bar(
#                 y=province,
#                 x=list(province.index),
#                 marker_color=colors
#             )])
# #fig.update_layout(yaxis_type="log")
# fig.update_layout(barmode='group')
# fig.show()
# plot(fig)
#province.plot(kind='bar', label='Confirmed',logy=True,figsize=(20,10), stacked=True,\
 #             title='China Province  with novel covid-19')
#plt.ylabel('Total patient')