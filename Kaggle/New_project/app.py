# -*- coding: utf-8 -*-
"""
Created on Thu Nov  4 13:05:36 2021

@author: pinak
"""

from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

if __name__ == "__main__":
    #app.run(host='127.0.0.1', port=8001, debug=True)
	app.run(debug=False)