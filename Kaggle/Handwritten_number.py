# -*- coding: utf-8 -*-
"""
Created on Thu May 14 18:03:52 2020

@author: 607144701
"""

import keras
from keras.datasets import mnist
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dense, Flatten
from keras.models import Sequential
import pandas as pd
import numpy as np
# Loading the dataset and perform splitting
# (x_train, y_train), (x_test, y_test) = mnist.load_data()
# # Peforming reshaping operation
# x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
# x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

x_train = pd.read_csv('C:/Pinak/Mygit/digit-recognizer/train.csv')
x_train.columns
y_train = x_train['label']
x_train.drop(['label'],axis=1,inplace=True)
x_train = x_train.to_numpy().reshape(42000,28,28,1)
y_train = y_train.to_numpy()


x_final_test = pd.read_csv('C:/Pinak/Mygit/digit-recognizer/test.csv')
x_final_test = x_final_test.to_numpy().reshape(28000,28,28,1)


x_train, x_validation = x_train[:21000,:], x_train[21000:,:]
y_train, y_validation = y_train[:21000], y_train[21000:]

# Normalization
x_train = x_train / 255
x_validation = x_validation / 255

# One Hot Encoding
y_train = keras.utils.to_categorical(y_train, 10)
y_validation = keras.utils.to_categorical(y_validation, 10)
#y_test = keras.utils.to_categorical(y_test, 10)
# Building the Model Architecture

model = Sequential()
# Select 6 feature convolution kernels with a size of 5 * 5 (without offset), and get 66 feature maps. The size of each feature map is 32−5 + 1 = 2832−5 + 1 = 28.
# That is, the number of neurons has been reduced from 10241024 to 28 ∗ 28 = 784 28 ∗ 28 = 784.
# Parameters between input layer and C1 layer: 6 ∗ (5 ∗ 5 + 1)
model.add(Conv2D(6, kernel_size=(5, 5), activation='relu', input_shape=(28, 28, 1)))
# The input of this layer is the output of the first layer, which is a 28 * 28 * 6 node matrix.
# The size of the filter used in this layer is 2 * 2, and the step length and width are both 2, so the output matrix size of this layer is 14 * 14 * 6.
model.add(MaxPooling2D(pool_size=(2, 2)))
# The input matrix size of this layer is 14 * 14 * 6, the filter size used is 5 * 5, and the depth is 16. This layer does not use all 0 padding, and the step size is 1.
# The output matrix size of this layer is 10 * 10 * 16. This layer has 5 * 5 * 6 * 16 + 16 = 2416 parameters
model.add(Conv2D(16, kernel_size=(5, 5), activation='relu'))
# The input matrix size of this layer is 10 * 10 * 16. The size of the filter used in this layer is 2 * 2, and the length and width steps are both 2, so the output matrix size of this layer is 5 * 5 * 16.
model.add(MaxPooling2D(pool_size=(2, 2)))
# The input matrix size of this layer is 5 * 5 * 16. This layer is called a convolution layer in the LeNet-5 paper, but because the size of the filter is 5 * 5, #
# So it is not different from the fully connected layer. If the nodes in the 5 * 5 * 16 matrix are pulled into a vector, then this layer is the same as the fully connected layer.
# The number of output nodes in this layer is 120, with a total of 5 * 5 * 16 * 120 + 120 = 48120 parameters.
model.add(Flatten())
model.add(Dense(120, activation='relu'))
# The number of input nodes in this layer is 120 and the number of output nodes is 84. The total parameter is 120 * 84 + 84 = 10164 (w + b)
model.add(Dense(84, activation='relu'))
# The number of input nodes in this layer is 84 and the number of output nodes is 10. The total parameter is 84 * 10 + 10 = 850
model.add(Dense(10, activation='softmax'))
model.compile(loss=keras.metrics.categorical_crossentropy, optimizer=keras.optimizers.Adam(), metrics=['accuracy'])
model.fit(x_train, y_train, batch_size=128, epochs=20, verbose=1, validation_data=(x_validation, y_validation))
score = model.evaluate(x_validation, y_validation)
print('Test Loss:', score[0])
print('Test accuracy:', score[1])

y_final_test = model.predict(x_final_test)
y_final_test=y_final_test.round()


class_labels = np.argmax(y_final_test, axis=1)
Final_output = pd.DataFrame(data = class_labels,  index=[i+1 for i in range(class_labels.shape[0])],columns=["Label"])
Final_output['ImageId'] = Final_output.index
Final_output.to_csv(r'C:/Pinak/Mygit/digit-recognizer/final_v1.csv' ,index = None, header=True)
# def get_index(row):
#     return(row.index[row.apply(lambda x: x==1)][0])

# df = pd.DataFrame()
# df['Label'] = y_final_test.apply(lambda row:get_index(row), axis=1)




import numpy as np
from keras.preprocessing import image
test_image = image.load_img('C:/Pinak/Mygit/digit-recognizer/eight.png',color_mode="grayscale", target_size = (28,28))
test_image = image.img_to_array(test_image)

test_image = np.expand_dims(test_image, axis = 0)
result = model.predict(test_image)
result_labels = np.argmax(result, axis=1)



#Save Data



from keras.models import load_model
model.save('model.h5')
new_model = load_model('model.h5')
# With saved model

import numpy as np
from keras.preprocessing import image
test_image = image.load_img('C:/Pinak/Mygit/digit-recognizer/t.jpg',color_mode="grayscale", target_size = (28,28))
test_image = image.img_to_array(test_image)

test_image = np.expand_dims(test_image, axis = 0)
result = model.predict(test_image)
result_labels = np.argmax(result, axis=1)
